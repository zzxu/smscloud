<?php
namespace Zzxu\Smscloud\Gateways;
use Zzxu\Smscloud\Factory;

class Dh3tGateway extends Gateway
{
    protected $random;
    public function send($mobile, $content='')
    {
        $this->setVerifyCode($mobile);
        $this->url = 'http://www.dh3t.com/json/sms';
        // 发送数据包json格式：{"account":"8528","password":"e717ebfd5271ea4a98bd38653c01113d","msgid":"2c92825934837c4d0134837dcba00150","phones":"15711666132","content":"您好，您的手机验证码为：430237。","sign":"【8528】","subcode":"8528","sendtime":"201405051230"}
        $data = [
            'account' => $this->config['account'],
            'password' => $this->config['password'],
            'msgid' => uniqid(rand(), true),
            'phones' => $mobile,
            'content' => empty($content) ? $this->content : $content,
            'sign' => $this->config['sign'],
            'subcode' => $this->config['subcode'],
            'sendtime' => date('YmdHi', time()),
        ] ;
        return $this->httpPostJson($this->url . "/Submit", json_encode($data));
    }
    public function response($response)
    {
        if ($response) {
            $result = json_decode($response, true);
            if ($result['ActionStatus'] != 'FAIL') {
                return json_encode(
                    [
                        'status'=>1,
                        'message'=>'短信发送成功'
                    ]
                );
            } else {
                return json_encode(
                    [
                        'status'=>0,
                        'message'=>'短信发送失败,错误码：'.$result['ErrorCode'].',错误信息：'.$result['ErrorInfo']
                    ]
                );
            }
        } else {
            return json_encode(['status'=>0,'message'=>'Http请求错误']);
        }
    }
}

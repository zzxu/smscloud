<?php
namespace Zzxu\Smscloud\Gateways;
use Zzxu\Smscloud\Factory;

class KjcxGateway extends Gateway
{
    protected $random;
    public function send($mobile, $content='')
    {
        $this->setVerifyCode($mobile);
        $this->url = 'http://160.19.212.218:8080/eums/utf8/send_strong.do';

        $seed = date('YmdHis', time());
        $key = md5(md5($this->config['password']) . $seed);
        $content = empty($content) ? $this->content : $content;
        $content = $this->config['sign'] . $content;
    
        $data = [
            'name' => $this->config['account'],
            'dest' => $mobile,
            'content' => $content,
            'key' => $key,
            'seed' => $seed,
        ] ;
        return $this->curl($this->url, $data);
    }
    public function response($response)
    {
        if ($response) {
            $result = explode(':', $response);
            if ($result[0] == 'success') {
                return json_encode(
                    [
                        'status'=>1,
                        'message'=>'短信发送成功'
                    ]
                );
            } else {
                return json_encode(
                    [
                        'status'=>0,
                        'message'=>'短信发送失败,错误码：'.$result[1].',错误信息：'. $this->getErrMsg($result[1]),
                    ]
                );
            }
        } else {
            return json_encode(['status'=>0,'message'=>'Http请求错误']);
        }
    }

    function getErrMsg($code)
    {
        $err = [
            '101' => '缺少name参数',
            '102' => '缺少seed参数',
            '103' => '缺少key参数',
            '104' => '缺少dest参数',
            '105' => '缺少content参数',
            '106' => 'seed错误',
            '107' => 'key错误',
            '108' => 'ext错误',
            '109' => '内容超长',
            '110' => '模板未备案',
            '111' => '无签名',
            '113' => '签名超长',
            '114' => '定时参数格式错',
            '115' => '定时时间范围错',
            '201' => '无对应账户',
            '202' => '账户暂停',
            '203' => '账户删除',
            '204' => '账户IP没备案',
            '205' => '账户无余额',
            '206' => '密码错误',
            '301' => '无对应产品',
            '302' => '产品暂停',
            '303' => '产品删除',
            '304' => '产品不在服务时间',
            '305' => '无匹配通道',
            '306' => '通道暂停',
            '307' => '通道已删除',
            '308' => '通道不在服务时间',
            '309' => '该账户未提供短信服务',
            '310' => '该账户未提供彩信服务',
            '401' => '屏蔽词',
            '500' => '查询间隔太短',
            '999' => '其他错误',
        ];

        return $err[$code];
    }
}
